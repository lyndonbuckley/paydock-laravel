<?php

return [
	'sandbox_baseuri' => 'https://api-sandbox.paydock.com',
	'production_baseuri' => 'https://api.paydock.com',
	'currency' => 'AUD',
	'environment' => env('PAYDOCK_ENVIRONMENT'),
	'secret_key' => env('PAYDOCK_SECRET_KEY'),
	'public_key' => env('PAYDOCK_PUBLIC_KEY'),
	'gateway_id' => env('PAYDOCK_GATEWAY_ID')
];
