<?php

namespace CrowdConnect\Paydock;

use CrowdConnect\Paydock\Traits\ConsumeApiTrait;

class Vault {
	
	use ConsumeApiTrait;
	
	public function create($ccNumber, $ccYear, $ccMonth, $ccName, $token = ''){
		
		$body = json_encode([
			'card_name' => $ccName,
			'card_number' => $ccNumber,
			'expire_month' => $ccMonth,
			'expire_year' => $ccYear,
			'token' => $token,
		]);
		
		return $this->performRequest('POST', '/v1/vault/payment_sources',$body,true,false);

	}
	
	public function createFromToken($token){
		
		$body = json_encode([
			'token' => $token
		]);
		
		return $this->performRequest('POST', '/v1/vault/payment_sources',$body,true,false);

	}
	
	public function deleteToken($token){
		
		return $this->performRequest('DELETE', '/v1/vault-tokens/'.$token,'',true,false);

	}

}