<?php

namespace CrowdConnect\Paydock;

use CrowdConnect\Paydock\Traits\ConsumeApiTrait;

class Customer {
	
	use ConsumeApiTrait;

	public function create($token, $firstname = '', $lastname = '', $email = '', $phone = ''){
		
		$body = json_encode([
			'token' => $token,
			'first_name' => $firstname,
			'last_name' => $lastname,
			'email' => $email,
			'phone' => $phone,
		]);
		
		return $this->performRequest('POST', '/v1/customers',$body,true,false);

	}
	
	public function update($customerid, $token){
		
		// $gatewayId = config('paydock.gateway_id');
		
		$body = json_encode([
			'token' => $token,
		]);
		
		return $this->performRequest('POST', '/v1/customers/'.$customerid,$body,true,false);

	}
	
		
	public function get($customerid){
		
		$body = '';
		
		return $this->performRequest('GET', '/v1/customers/'.$customerid,$body,true,false);

	}
	
}